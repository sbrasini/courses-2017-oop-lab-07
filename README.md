# Instructions

Every exercise contains a `README.md` file with instructions on what to do.
Solve the exercise in the following order: 

1. `anonymous1`
2. `enum1`
3. `nesting1`
4. `enum2`
5. `nesting2`
